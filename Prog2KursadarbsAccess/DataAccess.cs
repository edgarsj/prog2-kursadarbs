﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog2KursadarbsAccess
{
    class DataAccess
    {
        
        public static string GetConnectionString(string id = "Prog2KursadarbsAccess.Properties.Settings.edgars_jekabsons_donationsConnectionString")
        {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }

        public static void AddPerson(string code, string full_name) {
            
            using (OleDbConnection conn = new OleDbConnection(GetConnectionString()))
            {
                using (OleDbCommand cmd = conn.CreateCommand())
                {
                    conn.Open();
                    cmd.CommandText = "insert into Person ([FullName],[Code]) values (?,?)";
                    cmd.Parameters.AddWithValue("@fullname", full_name);
                    cmd.Parameters.AddWithValue("@code", code);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }

            }

        }
        public static int AddCause(string cause)
        {

            using (OleDbConnection conn = new OleDbConnection(GetConnectionString()))
            {
                using (OleDbCommand cmd = conn.CreateCommand())
                {
                    conn.Open();
                    cmd.CommandText = "insert into Cause ([CauseName]) values (?)";
                    cmd.Parameters.AddWithValue("@cause", cause);
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "Select @@Identity";
                    var ID = (int)cmd.ExecuteScalar();
                    conn.Close();
                    return ID;
                }

            }
            
        }
        public static void AddDonation(int personId, int causeId, DateTime paymentDate, Decimal amount)
        {

            using (OleDbConnection conn = new OleDbConnection(GetConnectionString()))
            {
                using (OleDbCommand cmd = conn.CreateCommand())
                {
                    conn.Open();
                    cmd.CommandText = "insert into Donation ([PersonId], [CauseId], [PaymentDate], [Amount]) values (?, ?, ?, ?)";
                    cmd.Parameters.AddWithValue("@personId", personId);
                    cmd.Parameters.AddWithValue("@causeId", causeId);
                    cmd.Parameters.AddWithValue("@paymentDate", paymentDate.ToString("yyyy-MM-dd"));
                    cmd.Parameters.AddWithValue("@amount", amount);
                    cmd.ExecuteNonQuery();
                }

            }

        }

    }
}
