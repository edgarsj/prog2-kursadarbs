﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prog2KursadarbsAccess
{
    public partial class AddDonationForm : Form
    {
        public AddDonationForm()
        {
            InitializeComponent();
        }

        private void AddDonationForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'donationsDataSet.Cause' table. You can move, or remove it, as needed.
            this.causeTableAdapter.Fill(this.donationsDataSet.Cause);
            // TODO: This line of code loads data into the 'donationsDataSet.Person' table. You can move, or remove it, as needed.
            this.personTableAdapter.Fill(this.donationsDataSet.Person);

        }

        private void btnNewPerson_Click(object sender, EventArgs e)
        {
            using (AddPersonForm personForm = new AddPersonForm())
            {
                if (personForm.ShowDialog() == DialogResult.OK)
                {
                    this.personTableAdapter.Fill(this.donationsDataSet.Person);
                }
            }
        }
        private void BtnNewCause_Click(object sender, EventArgs e)
        {
            var newId = DataAccess.AddCause(txtNewCause.Text);
            this.causeTableAdapter.Fill(this.donationsDataSet.Cause);
            this.comboBoxCause.SelectedValue = newId;
            
        }

        private void BtnSubmit_Click(object sender, EventArgs e)
        {
            //DataRow newDonation = donationsDataSet.Donation.NewDonationRow();
            //row.se

            //var newDonation = this.donationsDataSet.Donation.Rows.Add();
            /*newDonation.SetField(0, -1);
            newDonation.SetField(1, comboBoxPerson.SelectedValue);
            newDonation.SetField(2, comboBoxCause.SelectedValue);
            newDonation.SetField(3, dateTimePicker1.Value);
            newDonation.SetField(4, Convert.ToDecimal(txtAmount.Text));
            donationsDataSet.Donation.Rows.Add(newDonation);
*/
            //DataRow = newDonation.Table.Rows.Add({ -1, t });
            //DataAccess.AddDonation(txtCode.Text, txtFullName.Text);
            DataAccess.AddDonation((int)comboBoxPerson.SelectedValue, (int)comboBoxCause.SelectedValue, dateTimePicker1.Value, Convert.ToDecimal(txtAmount.Text));
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
