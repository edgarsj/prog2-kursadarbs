﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prog2KursadarbsAccess
{
    public partial class MainForm : Form

    {
        Bitmap bitmap;
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'donationsDataSet.Donation' table. You can move, or remove it, as needed.
            this.donationTableAdapter.Fill(this.donationsDataSet.Donation);
            this.loadDonations();

        }

        private void btnNewDonation_Click(object sender, EventArgs e)
        {
            using (AddDonationForm donationForm = new AddDonationForm())
            {
                if (donationForm.ShowDialog() == DialogResult.OK)
                {
                    this.donationTableAdapter.Fill(this.donationsDataSet.Donation);
                   
                }
            }
        }
        private void loadDonations() {

            using (OleDbConnection conn = new OleDbConnection(DataAccess.GetConnectionString()))
            {
                String sql = "SELECT [Donation.ID] as ID, [PaymentDate] as Datums, [Person.FullName] as Persona, [Person.Code] as Kods, [CauseName] as Cause, [Amount] as Summa FROM (Donation LEFT JOIN [Person] on Person.Id = Donation.PersonId) LEFT JOIN Cause on Cause.Id = Donation.CauseId";
                OleDbDataAdapter adapter = new OleDbDataAdapter(sql, conn);
                DataSet donations = new DataSet();
                //donations.
                adapter.Fill(donations, "Donations");
                donationsNewGridView.DataSource = donations.Tables[0];

                sql = " select CauseName as Cause, Sum(Amount) as Summa from donation left join cause on cause.id=donation.causeid  Group By CauseName";
                adapter = new OleDbDataAdapter(sql, conn);
                adapter.Fill(donations, "CauseReport");
                causesGridView.DataSource = donations.Tables[1];
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {

            bitmap = new Bitmap(this.donationsNewGridView.Width, this.donationsNewGridView.Height);
            donationsNewGridView.DrawToBitmap(bitmap, new Rectangle(0, 0, this.donationsNewGridView.Width, this.donationsNewGridView.Height));

            //Show the Print Preview Dialog.
            printPreviewDialog.Document = printCausesReport;
            printPreviewDialog.PrintPreviewControl.Zoom = 1;
            printPreviewDialog.ShowDialog();
        }
        private void BtnPrintCauses_Click(object sender, EventArgs e) {

            bitmap = new Bitmap(this.causesGridView.Width, this.causesGridView.Height);
            causesGridView.DrawToBitmap(bitmap, new Rectangle(0, 0, this.causesGridView.Width, this.causesGridView.Height));
            //Show the Print Preview Dialog.
            printPreviewDialog.Document = printCausesReport;
            printPreviewDialog.PrintPreviewControl.Zoom = 1;
            printPreviewDialog.ShowDialog();

        }

        private void PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            //Print the contents.
            e.Graphics.DrawImage(bitmap, 0, 0);
        }
    }
}
