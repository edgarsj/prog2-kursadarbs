﻿namespace Prog2KursadarbsAccess
{
    partial class AddDonationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelDate = new System.Windows.Forms.Label();
            this.labelPerson = new System.Windows.Forms.Label();
            this.labelCause = new System.Windows.Forms.Label();
            this.labelAmount = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.comboBoxPerson = new System.Windows.Forms.ComboBox();
            this.personBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.donationsDataSet = new Prog2KursadarbsAccess.donationsDataSet();
            this.personTableAdapter = new Prog2KursadarbsAccess.donationsDataSetTableAdapters.PersonTableAdapter();
            this.comboBoxCause = new System.Windows.Forms.ComboBox();
            this.causeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.causeTableAdapter = new Prog2KursadarbsAccess.donationsDataSetTableAdapters.CauseTableAdapter();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnNewPerson = new System.Windows.Forms.Button();
            this.btnNewCause = new System.Windows.Forms.Button();
            this.txtNewCause = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.personBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.donationsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.causeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.Location = new System.Drawing.Point(13, 33);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(43, 13);
            this.labelDate.TabIndex = 0;
            this.labelDate.Text = "Datums";
            // 
            // labelPerson
            // 
            this.labelPerson.AutoSize = true;
            this.labelPerson.Location = new System.Drawing.Point(13, 68);
            this.labelPerson.Name = "labelPerson";
            this.labelPerson.Size = new System.Drawing.Size(46, 13);
            this.labelPerson.TabIndex = 1;
            this.labelPerson.Text = "Persona";
            // 
            // labelCause
            // 
            this.labelCause.AutoSize = true;
            this.labelCause.Location = new System.Drawing.Point(13, 107);
            this.labelCause.Name = "labelCause";
            this.labelCause.Size = new System.Drawing.Size(38, 13);
            this.labelCause.TabIndex = 2;
            this.labelCause.Text = "Mērķis";
            // 
            // labelAmount
            // 
            this.labelAmount.AutoSize = true;
            this.labelAmount.Location = new System.Drawing.Point(13, 150);
            this.labelAmount.Name = "labelAmount";
            this.labelAmount.Size = new System.Drawing.Size(42, 13);
            this.labelAmount.TabIndex = 3;
            this.labelAmount.Text = "Summa";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(94, 27);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 4;
            // 
            // comboBoxPerson
            // 
            this.comboBoxPerson.DataSource = this.personBindingSource;
            this.comboBoxPerson.DisplayMember = "FullName";
            this.comboBoxPerson.FormattingEnabled = true;
            this.comboBoxPerson.Location = new System.Drawing.Point(94, 68);
            this.comboBoxPerson.Name = "comboBoxPerson";
            this.comboBoxPerson.Size = new System.Drawing.Size(121, 21);
            this.comboBoxPerson.TabIndex = 5;
            this.comboBoxPerson.ValueMember = "ID";
            // 
            // personBindingSource
            // 
            this.personBindingSource.DataMember = "Person";
            this.personBindingSource.DataSource = this.donationsDataSet;
            // 
            // donationsDataSet
            // 
            this.donationsDataSet.DataSetName = "donationsDataSet";
            this.donationsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // personTableAdapter
            // 
            this.personTableAdapter.ClearBeforeFill = true;
            // 
            // comboBoxCause
            // 
            this.comboBoxCause.DataSource = this.causeBindingSource;
            this.comboBoxCause.DisplayMember = "CauseName";
            this.comboBoxCause.FormattingEnabled = true;
            this.comboBoxCause.Location = new System.Drawing.Point(94, 104);
            this.comboBoxCause.Name = "comboBoxCause";
            this.comboBoxCause.Size = new System.Drawing.Size(121, 21);
            this.comboBoxCause.TabIndex = 6;
            this.comboBoxCause.ValueMember = "ID";
            // 
            // causeBindingSource
            // 
            this.causeBindingSource.DataMember = "Cause";
            this.causeBindingSource.DataSource = this.donationsDataSet;
            // 
            // causeTableAdapter
            // 
            this.causeTableAdapter.ClearBeforeFill = true;
            // 
            // txtAmount
            // 
            this.txtAmount.CausesValidation = false;
            this.txtAmount.Location = new System.Drawing.Point(94, 150);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(100, 20);
            this.txtAmount.TabIndex = 7;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(94, 209);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(121, 29);
            this.btnSubmit.TabIndex = 8;
            this.btnSubmit.Text = "Pievienot";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.BtnSubmit_Click);
            // 
            // btnNewPerson
            // 
            this.btnNewPerson.Location = new System.Drawing.Point(283, 68);
            this.btnNewPerson.Name = "btnNewPerson";
            this.btnNewPerson.Size = new System.Drawing.Size(102, 23);
            this.btnNewPerson.TabIndex = 9;
            this.btnNewPerson.Text = "Jauna persona";
            this.btnNewPerson.UseVisualStyleBackColor = true;
            this.btnNewPerson.Click += new System.EventHandler(this.btnNewPerson_Click);
            // 
            // btnNewCause
            // 
            this.btnNewCause.Location = new System.Drawing.Point(418, 104);
            this.btnNewCause.Name = "btnNewCause";
            this.btnNewCause.Size = new System.Drawing.Size(102, 23);
            this.btnNewCause.TabIndex = 10;
            this.btnNewCause.Text = "Pievienot";
            this.btnNewCause.UseVisualStyleBackColor = true;
            this.btnNewCause.Click += new System.EventHandler(this.BtnNewCause_Click);
            // 
            // txtNewCause
            // 
            this.txtNewCause.Location = new System.Drawing.Point(283, 104);
            this.txtNewCause.Name = "txtNewCause";
            this.txtNewCause.Size = new System.Drawing.Size(117, 20);
            this.txtNewCause.TabIndex = 11;
            // 
            // AddDonationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 333);
            this.Controls.Add(this.txtNewCause);
            this.Controls.Add(this.btnNewCause);
            this.Controls.Add(this.btnNewPerson);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.txtAmount);
            this.Controls.Add(this.comboBoxCause);
            this.Controls.Add(this.comboBoxPerson);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.labelAmount);
            this.Controls.Add(this.labelCause);
            this.Controls.Add(this.labelPerson);
            this.Controls.Add(this.labelDate);
            this.Name = "AddDonationForm";
            this.Text = "Pievienot ziedojumu";
            this.Load += new System.EventHandler(this.AddDonationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.personBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.donationsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.causeBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Label labelPerson;
        private System.Windows.Forms.Label labelCause;
        private System.Windows.Forms.Label labelAmount;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox comboBoxPerson;
        private donationsDataSet donationsDataSet;
        private System.Windows.Forms.BindingSource personBindingSource;
        private donationsDataSetTableAdapters.PersonTableAdapter personTableAdapter;
        private System.Windows.Forms.ComboBox comboBoxCause;
        private System.Windows.Forms.BindingSource causeBindingSource;
        private donationsDataSetTableAdapters.CauseTableAdapter causeTableAdapter;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnNewPerson;
        private System.Windows.Forms.Button btnNewCause;
        private System.Windows.Forms.TextBox txtNewCause;
    }
}