﻿namespace Prog2KursadarbsAccess
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.donationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.donationsDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.donationsDataSet = new Prog2KursadarbsAccess.donationsDataSet();
            this.btnNewDonation = new System.Windows.Forms.Button();
            this.donationTableAdapter = new Prog2KursadarbsAccess.donationsDataSetTableAdapters.DonationTableAdapter();
            this.donationsDataSet1 = new Prog2KursadarbsAccess.donationsDataSet();
            this.donationsNewGridView = new System.Windows.Forms.DataGridView();
            this.labelCausesReport = new System.Windows.Forms.Label();
            this.causesGridView = new System.Windows.Forms.DataGridView();
            this.printCausesReport = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog = new System.Windows.Forms.PrintPreviewDialog();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnPrintCauses = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.donationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.donationsDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.donationsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.donationsDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.donationsNewGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.causesGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // donationBindingSource
            // 
            this.donationBindingSource.DataMember = "Donation";
            this.donationBindingSource.DataSource = this.donationsDataSetBindingSource;
            // 
            // donationsDataSetBindingSource
            // 
            this.donationsDataSetBindingSource.DataSource = this.donationsDataSet;
            this.donationsDataSetBindingSource.Position = 0;
            // 
            // donationsDataSet
            // 
            this.donationsDataSet.DataSetName = "donationsDataSet";
            this.donationsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnNewDonation
            // 
            this.btnNewDonation.Location = new System.Drawing.Point(12, 24);
            this.btnNewDonation.Name = "btnNewDonation";
            this.btnNewDonation.Size = new System.Drawing.Size(110, 23);
            this.btnNewDonation.TabIndex = 1;
            this.btnNewDonation.Text = "Jauns ziedojums";
            this.btnNewDonation.UseVisualStyleBackColor = true;
            this.btnNewDonation.Click += new System.EventHandler(this.btnNewDonation_Click);
            // 
            // donationTableAdapter
            // 
            this.donationTableAdapter.ClearBeforeFill = true;
            // 
            // donationsDataSet1
            // 
            this.donationsDataSet1.DataSetName = "donationsDataSet";
            this.donationsDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // donationsNewGridView
            // 
            this.donationsNewGridView.AllowUserToAddRows = false;
            this.donationsNewGridView.AllowUserToDeleteRows = false;
            this.donationsNewGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.donationsNewGridView.Location = new System.Drawing.Point(12, 67);
            this.donationsNewGridView.Name = "donationsNewGridView";
            this.donationsNewGridView.ReadOnly = true;
            this.donationsNewGridView.Size = new System.Drawing.Size(807, 175);
            this.donationsNewGridView.TabIndex = 2;
            // 
            // labelCausesReport
            // 
            this.labelCausesReport.AutoSize = true;
            this.labelCausesReport.Location = new System.Drawing.Point(12, 266);
            this.labelCausesReport.Name = "labelCausesReport";
            this.labelCausesReport.Size = new System.Drawing.Size(111, 13);
            this.labelCausesReport.TabIndex = 3;
            this.labelCausesReport.Text = "Pārskats pēc mērķiem";
            // 
            // causesGridView
            // 
            this.causesGridView.AllowUserToAddRows = false;
            this.causesGridView.AllowUserToDeleteRows = false;
            this.causesGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.causesGridView.Location = new System.Drawing.Point(15, 292);
            this.causesGridView.Name = "causesGridView";
            this.causesGridView.ReadOnly = true;
            this.causesGridView.Size = new System.Drawing.Size(286, 150);
            this.causesGridView.TabIndex = 4;
            // 
            // printCausesReport
            // 
            this.printCausesReport.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintPage);
            // 
            // printPreviewDialog
            // 
            this.printPreviewDialog.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog.Enabled = true;
            this.printPreviewDialog.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog.Icon")));
            this.printPreviewDialog.Name = "printPreviewDialog";
            this.printPreviewDialog.Visible = false;
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(744, 24);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 5;
            this.btnPrint.Text = "Drukāt";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // btnPrintCauses
            // 
            this.btnPrintCauses.Location = new System.Drawing.Point(226, 261);
            this.btnPrintCauses.Name = "btnPrintCauses";
            this.btnPrintCauses.Size = new System.Drawing.Size(75, 23);
            this.btnPrintCauses.TabIndex = 6;
            this.btnPrintCauses.Text = "Drukāt";
            this.btnPrintCauses.UseVisualStyleBackColor = true;
            this.btnPrintCauses.Click += new System.EventHandler(this.BtnPrintCauses_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(862, 587);
            this.Controls.Add(this.btnPrintCauses);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.causesGridView);
            this.Controls.Add(this.labelCausesReport);
            this.Controls.Add(this.donationsNewGridView);
            this.Controls.Add(this.btnNewDonation);
            this.Name = "MainForm";
            this.Text = "Ziedojumi";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.donationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.donationsDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.donationsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.donationsDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.donationsNewGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.causesGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource donationsDataSetBindingSource;
        private donationsDataSet donationsDataSet;
        private System.Windows.Forms.BindingSource donationBindingSource;
        private donationsDataSetTableAdapters.DonationTableAdapter donationTableAdapter;
        private System.Windows.Forms.Button btnNewDonation;
        private donationsDataSet donationsDataSet1;
        private System.Windows.Forms.DataGridView donationsNewGridView;
        private System.Windows.Forms.Label labelCausesReport;
        private System.Windows.Forms.DataGridView causesGridView;
        private System.Drawing.Printing.PrintDocument printCausesReport;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnPrintCauses;
    }
}